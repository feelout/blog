
var PostsUserRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('user', params.id).then(function(user) {
            return {
                user: user,
                posts: user.get('posts')
            };
        });
    }
});

export default PostsUserRoute;
