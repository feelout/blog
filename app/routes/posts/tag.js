var PostsTagRoute = Ember.Route.extend({
    model:  function(params) {
        console.log('PostsTagRoute::model');
        return this.store.find('post').then(function(posts) {
            return {
                tag: params.tag,
                posts: posts.filter(function(post) {
                    return post.get('tags').indexOf(params.tag) !== -1;
                })
            };
        });
    }
});

export default PostsTagRoute;
