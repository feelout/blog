
export default Ember.Route.extend({
    beforeModel: function(transition) {
		var currentUser = this.controllerFor('application').get('currentUser');

		if(!currentUser) {
			console.log('Not logged in - can\'t create posts');
			transition.abort();
		}
        return true;
    }
});
