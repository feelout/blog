export default Ember.Route.extend({
    setupController: function(controller, model) {
        controller.set('model', model);
        this.store.find('user', 'admin').then(function(user) {
            controller.set('currentUser', user);
        });
    },

    actions: {
        deletePost: function(post) {
            this.get('controller').send('deletePost', post);
        }
    }
});
