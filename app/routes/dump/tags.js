var DumpTagsRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('tag');
    }
});

export default DumpTagsRoute;
