var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {
    this.route('component-test');
	this.route('helper-test');
	this.resource('posts', function() {
        this.route('new');
        this.route('post', { path: '/:id' });
        this.route('tag', { path: '/tag/:tag' });
        this.route('user', { path: '/user/:id' });
    });

	this.resource('users', function() {
		this.route('user', { path: '/:id' });
		this.route('new');
	});

    this.resource('dump', function() {
        this.route('tags');
        this.route('comments');
        this.route('users');
        this.route('posts');
    });
});

export default Router;
