
var User = DS.Model.extend({
    name:       DS.attr('string'),
    
    comments:   DS.hasMany('comment'),
    posts:      DS.hasMany('post', {})
});

User.FIXTURES = [
    {
        id: "admin",
        name: "admin",
        comments: [],
        posts: [1]
    },

    {
        id: "reader",
        name: "reader",
        comments: [],
        posts: []
    }
];

export default User;
