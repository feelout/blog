
var Comment = DS.Model.extend({
    post:       DS.belongsTo('post'),
    author:     DS.belongsTo('user'),
    content:    DS.attr('string'),
    date:       DS.attr('date')
});

Comment.FIXTURES = [];

export default Comment;
