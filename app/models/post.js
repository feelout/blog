
var Post = DS.Model.extend({
    title:          DS.attr('string'),
    text:           DS.attr('string'),
    author:         DS.belongsTo('user'),
    date:           DS.attr('date'),
    comments:       DS.hasMany('comment'),
    tags:           DS.attr()
});

Post.FIXTURES = [
    {
        id: 1,
        title: "First post",
        text: "This is the first post. And its second sentence.",
        author: "admin",
        date: new Date(),
        tags: ["foo", "bar"],
        comments: [] // !!
    }
];

export default Post;
