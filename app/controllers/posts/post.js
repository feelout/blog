
var PostsPostController = Ember.ObjectController.extend({
    needs: ['application'],
    newCommentText: '',

    actions: {
        submitComment: function() {
            var userName = this.get('controllers.application.currentUser.name');

            var self = this;

            this.store.find('post', this.get('model.id')).then(function(post) {
                console.log('Post title: ', post.get('title'));
                var record = self.store.createRecord('comment', {
                    post: post,
                    author: self.get('controllers.application.currentUser'),
                    content: self.get('newCommentText'),
                    date: new Date()
                });

                record.save().then(function(rec) {
                    console.log('save() successful');
                }, function() {
                    console.log('save() failed');
                });
            }, function() { console.log('Post not found: ', this.get('model.id')); });
        }
    }
});

export default PostsPostController;
