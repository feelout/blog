var PostsNewController = Ember.Controller.extend({
    needs: ["application"],
    title: "Untitled",
    text: "",
    tags: "foo",

    actions: {
        submit: function() {
            console.log("Submitting");

            var self = this;

            var user = this.get('controllers.application.currentUser');
            var tags = this.get('tags').split(/\s+/);

            console.log('Current user: ', user);
            console.log('Tags: ', tags);

            var post = {
                title: self.get('title'), 
                text: self.get('text'),
                author: user,
                date: new Date(),
                tags: tags
            };


            var postInStore = self.store.createRecord('post', post);
            console.log('Comments: ', postInStore.get('comments'));

            postInStore.save().then(function() {
                console.log('save() successful');

                self.transitionToRoute('posts.index');
            }, function() {
                console.log('save() failed');
            });
        }
    }
});

export default PostsNewController;
