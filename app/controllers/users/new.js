
var UsersNewController = Ember.ArrayController.extend({
	login: "",
	error: false,

	isValidLogin: function(login) {
		return login.trim() !== "" && login.indexOf(' ') === -1; // TODO: Check if exists
	},

	actions: {
		addUser: function() {
			var login = this.get('login');

			if(this.isValidLogin(login)) {
				this.set('error', false);

				var record = this.store.createRecord('user', {
					id: login,
					name: login
				});

				var self = this;

				record.save().then(function() {
					self.transitionToRoute('posts.index');	
				});
			} else {
				this.set('error', true);
			}
		}
	}
});

export default UsersNewController;
