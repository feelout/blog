var ApplicationController = Ember.Controller.extend({
    currentUser: null,
    username: '',
	error: false,
	errorMessage: '',

    actions: {
        logout: function() {
            console.log('Logging out');
            this.set('currentUser', null);
			this.set('error', false);
			this.set('errorMessage', '');
        },

        login: function() {
            var self = this;

            console.log('Logging in');

            self.store.find('user', self.get('username')).then(function(user) {
                console.log('Logged in as user ', user.get('id'));

                self.set('currentUser', user);
				self.set('error', false);
				self.set('errorMessage', 'test');

				console.log('in error? : ', self.get('error'));
            }, function() {
				console.log('Unknown user: ', self.get('username'));

				self.set('error', true);
				self.set('errorMessage', 'Unknown user');
			});
        },

        deletePost: function(post) {
            console.log('ApplicationController: deletePost');
            console.log('post: ', post);

            post.deleteRecord();
            post.save();
        }
    }
});

export default ApplicationController;
